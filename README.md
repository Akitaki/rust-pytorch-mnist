This is an end-to-end example for using

- torchscript jit model generated using a python script
- [tch](https://lib.rs/crates/tch), the Rust binding for PyTorch.
- cuda

to train and evaluate a custom module for MNIST.

# Usage

1. Download and unpack the MNIST dataset into `mnist/`.
2. Goto `models/` and run `generate_model.py`.
3. Run the main program by `cargo run`.

The rust toolchain and pytorch are needed.
With Nix, just do `nix-shell` and they should all be available.
